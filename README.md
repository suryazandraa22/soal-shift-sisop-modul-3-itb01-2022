# soal-shift-sisop-modul-3-ITB01-2022

# Kelompok ITB01 Sistem Operasi B

- I Putu Windy Arya Sagita 5027201071
- Fatchia Farhan 5027201044
- Surya Zandra Anggoro 5027201018

# Soal

Link soal : [Soal Shift 3](https://docs.google.com/document/d/1w3tYGgqIkHRFoqKJ9nKzwGLCbt30HM7S/edit)

---

# Daftar Isi
* [Soal 1](#user-content-soal-1)
* [Soal 2](#user-content-soal-2)
* [Soal 3](#user-content-soal-3)

## Soal 1

Oleh - 

### Narasi Soal
Novak adalah seorang mahasiswa biasa yang terlalu sering berselancar di internet. Pada suatu saat, Ia menemukan sebuah informasi bahwa ada suatu situs yang tidak memiliki pengguna. Ia mendownload berbagai informasi yang ada dari situs tersebut dan menemukan sebuah file dengan tulisan yang tidak jelas. Setelah diperhatikan lagi, kode tersebut berformat base64. Ia lalu meminta kepada anda untuk membuat program untuk memecahkan kode-kode di dalam file yang Ia simpan di drive dengan cara decoding dengan base 64. Agar lebih cepat, Ia sarankan untuk menggunakan thread.

### # A
Download dua file zip dan unzip file zip tersebut di dua folder yang berbeda dengan nama quote untuk file zip quote.zip dan music untuk file zip music.zip. Unzip ini dilakukan dengan bersamaan menggunakan thread.

**Penyelesaian A**
pada soal ini kita diminta untuk mendownload 2 file zip yang telah di sediakan pada gdrive yang telah di buat lalu kita di suruh untuk melakukan unzip pada kedua file
```c 
void makeDir(char *dirName) {
    int status;
    pid_t child_id;
    child_id = fork();
    if (child_id == 0) {
        char *argv[] = {"mkdir", "-p", dirName, NULL};
        execv("/usr/bin/mkdir", argv);
    } else {
        ((wait(&status)) > 0);
    }
}

void downloadQuote() {
    int status;
    pid_t child_id;
    child_id = fork();
    if (child_id == 0) {
        char *argv[] = {"wget", "--no-check-certificate", "https://drive.google.com/uc?id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt&export=download", "-O", "quote.zip", NULL};
        execv("/bin/wget", argv);
    } else {
        ((wait(&status)) > 0);
    }
}

void downloadMusic() {
    int status;
    pid_t child_id;
    child_id = fork();
    if (child_id == 0) {
        char *argv[] = {"wget", "--no-check-certificate", "https://drive.google.com/uc?id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1&export=download", "-O", "music.zip", NULL};
        execv("/bin/wget", argv);
    } else {
        ((wait(&status)) > 0);
    }
} 
```
code diatas digunakan untuk mendownload file yang telah disediakan pada gdrive

```c 
void *processUnzip(void *tipe) {
    int status;
    pid_t child_id;
    child_id = fork();
    if (child_id == 0) {
        char *argv[] = {"unzip", "music.zip", "-d", pathm, NULL};
        execv("/usr/bin/unzip", argv);
    } else {
        ((wait(&status)) > 0);
    }

    child_id = fork();
    if (child_id == 0) {
        char *argv[] = {"unzip", "quote.zip", "-d", pathq, NULL};
        execv("/usr/bin/unzip", argv);
    } else {
        ((wait(&status)) > 0);
    }

}
```
selanjutnya setelah medownload file kita menggunakan kode diatas untuk melakukan proses unzip pada kedua file tadi yang telah di download

Penyelesaian A

### # B
Decode semua file .txt yang ada dengan base 64 dan masukkan hasilnya dalam satu file .txt yang baru untuk masing-masing folder (Hasilnya nanti ada dua file .txt) pada saat yang sama dengan menggunakan thread dan dengan nama quote.txt dan music.txt. Masing-masing kalimat dipisahkan dengan newline/enter.

**Penyelesaian B**
pada soal kedua ini kita diminta untuk medecode semua file yang telah didownload tadi menggunakan base64 dan setelahnya kitadiminta untuk membuat file.txt baru untuk hasil dari decode yang telah dilakukan
```c
// base64 decode thanks/credit to John Schambers https://nachtimwald.com/tag/base64/
const char b64chars[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
int b64invs[] = {62, -1, -1, -1, 63, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -1, -1, -1, -1, -1, -1, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -1, -1, -1, -1, -1, -1, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51 };
size_t b64_decoded_size(const char *encd) {
    size_t len, res, i;

    if (encd == NULL) {
        return 0;
    }

    len = strlen(encd);
    res = len / 4 * 3;

    for (i = len; i-->0; ) {
        if (encd[i] == '=') {
            res--;
        } else {
            break;
        }
    }

    return res;
}

void b64_decodetable() {
    int inv[80];
    size_t i;

    memset(inv, -1, sizeof inv);
    for (i = 0; i < sizeof(b64chars) - 1; i++) {
        inv[b64chars[i] - 43] = i;
    }
}

int b64_validchar(char x) {
    int flag = 0;

    if (x >= '0' && x <= '9') {
        flag = 1;
    } else if (x >= 'A' && x <= 'Z') {
        flag = 1;
    } else if (x >= 'a' && x <= 'z') {
        flag = 1;
    } else if (x == '+' || x == '/' || x == '=') {
        flag = 1;
    }

    return flag;
}

void b64_decode(const char *encd, unsigned char *decd, size_t decdlen) {
    size_t len, i, j;
    int v;

    if (encd == NULL || decd == NULL) {
        return;
    }

    len = strlen(encd);
    if (decdlen < b64_decoded_size(encd) || len % 4 != 0) {
        return;
    }

    for (i = 0; i < len; i++) {
        if (b64_validchar(encd[i]) == 0) {
            return;
        }
    }

    for (i=0, j=0; i<len; i+=4, j+=3) {
		v = b64invs[encd[i]-43];
		v = (v << 6) | b64invs[encd[i+1]-43];
		v = encd[i+2]=='=' ? v << 6 : (v << 6) | b64invs[encd[i+2]-43];
		v = encd[i+3]=='=' ? v << 6 : (v << 6) | b64invs[encd[i+3]-43];

		decd[j] = (v >> 16) & 0xFF;
		if (encd[i+2] != '=')
			decd[j+1] = (v >> 8) & 0xFF;
		if (encd[i+3] != '=')
			decd[j+2] = v & 0xFF;
	}
}
```
code diatas ini digunakan untuk me-decode hasil file.txt yang telah kita download tadi

```c
void processText() {
    char textType[2][10] = {"music", "quote"};

    struct dirent *dp;
    DIR *dir;

    for (int i = 0; i < 2; i++) {
        char patht[100];
        if (strcmp(textType[i], "music") == 0) {
            strcpy(patht, pathm);
        } else {
            strcpy(patht, pathq);
        }

        dir = opendir(patht);
        if (!dir) {
            break;
        }

        int data = 0;
        while ((dp = readdir(dir)) != NULL) {
            if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0) {
                char temp[100];
                char *temp1;
                char *temp2;
                size_t declen;
                strcpy(temp, patht);
                strcat(temp, "/");
                strcat(temp, dp->d_name);
                // printf("%s\n", temp);
                FILE *fptr;
                fptr = fopen(temp, "a+");
                if (strcmp(textType[i], "music") == 0) {
                    fscanf(fptr, "%s", encodedm[data]);
                    // printf("%s 1\n", encodedm[data]);
                    temp1 = encodedm[data];
                    // printf("%s 2\n", encodedm[data]);
                    declen = b64_decoded_size(temp1);
                    // printf("%s 3\n", encodedm[data]);
                    temp2 = malloc(declen);
                    // printf("%s 4\n", encodedm[data]);
                    b64_decode(temp1, (unsigned char *)temp2, declen);
                    // printf("%s 5\n", encodedm[data]);
                    temp2[declen] = '\0';
                    // printf("%s 6\n", encodedm[data]);
                    strcpy(decodedm[data], temp2);
                    // printf("%s 7\n", encodedm[data]);
                    free (temp2);
                    // printf("\nmusic\n");
                    // printf("%s\n", decodedm[data]);
                } else {
                    fscanf(fptr, "%s", encodedq[data]);
                    temp1 = encodedq[data];
                    // printf("%s 2\n", encodedq[data]);
                    declen = b64_decoded_size(temp1);
                    // printf("%s 3\n", encodedq[data]);
                    temp2 = malloc(declen);
                    // printf("%s 4\n", encodedq[data]);
                    b64_decode(temp1, (unsigned char *)temp2, declen);
                    // printf("%s 5\n", encodedq[data]);
                    temp2[declen] = '\0';
                    // printf("%s 6\n", encodedq[data]);
                    strcpy(decodedq[data], temp2);
                    // printf("%s 7\n", encodedq[data]);
                    free (temp2);
                    // printf("\nquote\n");
                    // printf("%s\n", decodedq[data]);
                }
                fclose(fptr);
            }
            data++;
        }
        closedir(dir);
    }
```
code diatas ini berfungsi untuk melakukan proses decode pada file.txt


Penyelesaian B

### # C
Pindahkan kedua file .txt yang berisi hasil decoding ke folder yang baru bernama hasil.

**Penyelesaian C**
pada soal ini setelah me-decode yang telah didownload tadi kita di minta untuk memindahkan file tersebut kedalam folder baru yang bernama hasil
```c
void *moveText(void *pathl) {
    char pathh[100];
    char textType[2][10] = {"music", "quote"};
    strcpy(pathh, pathl);
    
    for (int i = 0; i < 2; i++) {
        char temp[100];
        strcpy(temp, pathh);
        if (strcmp(textType[i], "music") == 0) {
            strcat(temp, "/");
            strcat(temp, "music.txt");
        } else {
            strcat(temp, "/");
            strcat(temp, "quote.txt");
        }

        FILE *fptr;
        fptr = fopen(temp, "a+");
        // printf("%s\n", textType[i]);
        if (strcmp(textType[i], "music") == 0) {
            for (int j = 0; j < 11; j++) {
                if (strlen(decodedm[j]) > 3) {
                    fprintf(fptr, "%s\n", decodedm[j]);
                    // printf("%s\n", decodedm[j]);
                    // printf("%d\n", j);
                }
            }
        } else {
            for (int j = 0; j < 11; j++) {
                if (strlen(decodedq[j]) > 3) {
                    fprintf(fptr, "%s\n", decodedq[j]);
                }
            }
        }
    }
}
```
code diatas digunakan untuk memindahan hasil file decode yang telah dilakukan ke dalam file baru yang diminta
Penyelesaian C

### # D
Folder hasil di-zip menjadi file hasil.zip dengan password 'mihinomenest[Nama user]'. (contoh password : mihinomenestnovak)

**Penyelesaian D**
pada soal ini seletah memindahkan file tersebut selanjutnyakita di minta melakukan zip pada file tadi dan meberikan password pada file zip yang baru dibuat
```c
void zip() {
    chdir(path);
    // char s[100];
    // printf("%s\n", getcwd(s, 100));
    int status;
    pid_t child_id;
    child_id = fork();
    if (child_id = 0) {
        char *argv[] = {"zip", "-P", "mihinomenestwindy", "-r", "hasil.zip", "/home/ubuntu/modul3/hasil/", NULL};
        execv("/usr/bin/zip", argv);
    }
}
```
code diatas digunakan untuk mengzip file yang telah selesai dan memberikan password pada file zip dengan ketentuan password  yaitu "mihinomenest(nama anggota kelompok)"
Penyelesaian D

### # E
Karena ada yang kurang, kalian diminta untuk unzip file hasil.zip dan buat file no.txt dengan tulisan 'No' pada saat yang bersamaan, lalu zip kedua file hasil dan file no.txt menjadi hasil.zip, dengan password yang sama seperti sebelumnya.

**Penyelesaian E**

Penyelesaian E

### # Kendala
Kendala pengerjaan soal 1

---

## Soal 2

Oleh - 

### Narasi Soal
Bluemary adalah seorang Top Global 1 di salah satu platform online judge. Suatu hari Ia ingin membuat online judge nya sendiri, namun dikarenakan Ia sibuk untuk mempertahankan top global nya, maka Ia meminta kamu untuk membantunya dalam membuat online judge sederhana. Online judge sederhana akan dibuat dengan sistem client-server dengan beberapa kriteria sebagai berikut:

### # A
Pada saat client terhubung ke server, terdapat dua pilihan pertama yaitu register dan login. Jika memilih register, client akan diminta input id dan passwordnya untuk dikirimkan ke server. Data input akan disimpan ke file **users.txt** dengan format **username:password**. Jika client memilih login, server juga akan meminta client untuk input id dan passwordnya lalu server akan mencari data di **users.txt** yang sesuai dengan input client. Jika data yang sesuai ditemukan, maka client dapat login dan dapat menggunakan command-command yang ada pada sistem. Jika tidak maka server akan menolak login client. Username dan password memiliki kriteria sebagai berikut:
* Username unique (tidak boleh ada user yang memiliki username yang sama)
* Password minimal terdiri dari 6 huruf, terdapat angka, terdapat huruf besar dan kecil

**Penyelesaian A**

Penyelesaian A

### # B
Sistem memiliki sebuah database pada server untuk menampung problem atau soal-soal yang ada pada online judge. Database ini bernama **problems.tsv** yang terdiri dari **judul problem dan author problem (berupa username dari author), yang dipisah dengan \t**. File otomatis dibuat saat server dijalankan.

**Penyelesaian B**

Penyelesaian B

### # C
**Client yang telah login**, dapat memasukkan command yaitu ‘add’ yang berfungsi untuk menambahkan problem/soal baru pada sistem. Saat client menginputkan command tersebut, server akan meminta beberapa input yaitu:
* Judul problem (unique, tidak boleh ada yang sama dengan problem lain)
* Path file description.txt pada client (file ini berisi deskripsi atau penjelasan problem)
* Path file input.txt pada client (file ini berguna sebagai input testcase untuk menyelesaikan problem)
* Path file output.txt pada client (file ini berguna untuk melakukan pengecekan pada submission client terhadap problem)
Seluruh file akan disimpan oleh server ke dalam folder dengan nama `<judul-problem>` yang di dalamnya terdapat file description.txt, input.txt dan output.txt. Penambahan problem oleh client juga akan mempengaruhi file **problems.tsv**.


**Penyelesaian C**

Penyelesaian C

### # D
**Client yang telah login**, dapat memasukkan command ‘see’ yang berguna untuk menampilkan seluruh judul problem yang ada beserta authornya(author merupakan username client yang menambahkan problem tersebut).

**Penyelesaian D**

Penyelesaian D

### # E
**Client yang telah login**, dapat memasukkan command ‘download `<judul-problem>`’ yang berguna untuk mendownload file description.txt dan input.txt yang berada pada folder pada server dengan nama yang sesuai dengan argumen kedua pada command yaitu `<judul-problem>`. Kedua file tersebut akan disimpan ke folder dengan nama `<judul-problem>` di client.

**Penyelesaian E**

Penyelesaian E

### # F
**Client yang telah login**, dapat memasukkan command ‘submit `<judul-problem> <path-file-output.txt>`’.  Command ini berguna untuk melakukan submit jawaban dari client terhadap problem tertentu. Algoritma yang dijalankan adalah client akan mengirimkan file output.txt nya melalui argumen ke 3 pada command, lalu server akan menerima dan membandingkan isi file output.txt yang telah dikirimkan oleh client dan output.txt yang ada pada folder dengan nama yang sesuai dengan argumen ke 2 pada command. Jika file yang dibandingkan sama, maka server akan mengirimkan pesan “AC” dan jika tidak maka server akan mengeluarkan pesan “WA”.

**Penyelesaian F**

Penyelesaian F

### # G
Server dapat menangani multiple-connection. Dimana jika terdapat 2 atau lebih client yang terhubung ke server, maka harus menunggu sampai client pertama keluar untuk bisa melakukan login dan mengakses aplikasinya.

**Penyelesaian G**

Penyelesaian G

### # Kendala
Kendala pengerjaan soal 2

---

## Soal 3

Oleh - I Putu Windy Arya Sagita

### Narasi Soal
Nami adalah seorang pengoleksi harta karun handal. Karena Nami memiliki waktu luang, Nami pun mencoba merapikan harta karun yang dimilikinya berdasarkan jenis/tipe/kategori/ekstensi harta karunnya. Setelah harta karunnya berhasil dikategorikan, Nami pun mengirimkan harta karun tersebut ke kampung halamannya.

### # A
Hal pertama yang perlu dilakukan oleh Nami adalah mengextract zip yang diberikan ke dalam folder `/home/[user]/shift3/`. Kemudian working directory program akan berada pada folder `/home/[user]/shift3/hartakarun/`. Karena Nami tidak ingin ada file yang tertinggal, program harus mengkategorikan seluruh file pada working directory secara rekursif

**Penyelesaian A**

Pada soal bagian A ini, kita diminta untuk mengextract zip file yang diberikan pada drive soal, karena adanya catatan pada soal yang melarang untuk digunakannya fungsi fork(), execv() sehingga extract file zip ini akan dilakukan secara manual. Berikut adalah hasil extractnya.
![Hasil Extract](image/soal3a.1.png)
Setelah file diextract, maka kita akan melakukan pengkategorian seluruh file yang ada. Kami di sini menggunakan template listfilerecursively yang ada pada modul 2. Kami melakukan sedikit perubahan untuk menyesuaikan dengan permintaan soal. Berikut adalah kodenya.
```c
void listFilesRecursively(char *basePath) {
    char path2[1000];
    struct dirent *dp;
    DIR *dir = opendir(basePath);

    if (!dir) {
        return;
    }

    while ((dp = readdir(dir)) != NULL) {
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0) {
            strcpy(path2, basePath);
            strcat(path2, dp->d_name);
            if (dp->d_type == DT_DIR) {
            	strcat(path2, "/");	
            }
            char buffer[1000];
            strcpy(buffer, dp->d_name);
            if (buffer[0] == '.') {
                createDir("hidden");
                char hidd[1000];
                strcpy(hidd, "hidden/");
                strcat(hidd, dp->d_name);
                rename(path2, hidd);
                // printf("%s\n", hidd);
            } else {
                char loc[1000];
                strcpy(loc, path2);
                //printf("%s\n", loc);
                strcpy(data[t1], loc);
                //printf("%s\n", data[t1]);
                t1++;
            }
            listFilesRecursively(path2);
        }
    }
    closedir(dir);
}
```
Pada kode di atas, kami akan melakukan read directory dan memasukan hasil lokasi directori dari file-file ke array `data` dan akan mengecek apakah ada fila yang tersembunyi atau hidden untuk kemudian diproses. Pada awal loop kami juga menambahkan kondisi `if` yang berfungsi untuk mengecek apakah file yang di-read saat ini adalah sebuah directori/folder atau tidak, jika merupakan sebuah folder maka kami menambahkan `/`. Ini kami lakukan karena ada beberapa file yang ada di dalam subfolder lagi. Penjelasan selanjutnya akan ada pada penyelesaian poin B.

### # B
Semua file harus berada di dalam folder, jika terdapat file yang tidak memiliki ekstensi, file disimpan dalam folder “Unknown”. Jika file hidden, masuk folder “Hidden”.

**Penyelesaian B**

Dari hasil array `data` yang sudah didapat pada soal bagian A, saat ini kita akan memproses file-file itu untuk dimasukan ke dalam folder sesuai dengan ekstensinya. Berikut adalah kode yang kami gunakan.
```c
void *processFiles(void *loc) {
    char *fileloc, *ext, *filename, *token;
    char str1[100], str2[100], ext1[50], fname[100];
    char fileloc2[1000];

    struct dirent *dp;
    fileloc = (char *) loc;
    //printf("%s\n", fileloc);
    DIR *dir = opendir(fileloc);

    if (dir == NULL) {
        strcpy(str1, fileloc);
        ext = strrchr(str1, '/');
        token = strtok(str1, ".");
        token = strtok(NULL, "");
        if (ext != NULL) {
            if (ext[1] == "." && token != NULL) {
                strcpy(ext1, "hidden");
            } else if (token == NULL) {
                strcpy(ext1, "unknown");
            } else {
                strcpy(ext1, token);
                for (int i = 0; i < strlen(ext1); i++) {
                    ext1[i] = tolower(ext1[i]);
                }
            }
        }
        
        strcpy(str2, fileloc);
        filename = strrchr(str2, '/');
        token = strtok(filename, "/");
        token = strtok(token, ".");
        strcpy(fname, token);

        createDir(ext1);

        strcpy(fileloc2, fileloc);
        // printf("%s\n", fileloc);
        char temp[1000];
        if (strcmp(ext1, "unknown") == 0 || strcmp(ext1, "hidden") == 0) {
            strcpy(temp, ext1);
            strcat(temp, "/");
            strcat(temp, fname);
        } else {
            strcpy(temp, ext1);
            strcat(temp, "/");
            strcat(temp, fname);
            strcat(temp, ".");
            strcat(temp, ext1);
        }
        rename(fileloc2, temp);
        // printf("%s %s %s\n", ext1, fname, temp);
    }
}
```
Pada kode di atas, kami pertama-tama akan melakukan pemotongan string namafile untuk mencari ekstensi dari file tersebut berdasarkan tanda titik pertama yang mencul. Jika misalnya tanda titiknya ada paling depan, maka itu masuk ke file hidden, jika tidak ditemukan adanya tanda titik, maka itu adalah file unknown, dan jika ditemukan tanda titik, maka ekstensi akan diambil mulai dari setelah tanda titik tersebut. Kemudian, kami akan melakukan trim string kembali untuk mendapatkan nama file. Setelah itu, kami akan membuat directory baru sesuai dengan ekstensi yang ada. Kemudian, kami akan memindahkan file tersebut ke dalam foler seusia ekstensinya yang sudah dibuat tadi. Berikut adalah hasilnya.
![Hasil Pengkategorian](image/soal3a.2.png)
Berikut adalah hasil untuk folder hidden.
![Hasil Hidden](image/soal3a.3.png)
Berikut adalah hasil untuk folder unknown.
![Hasil Unknown](image/soal3a.4.png)

### # C
Agar proses kategori bisa berjalan lebih cepat, setiap 1 file yang dikategorikan dioperasikan oleh 1 thread.

**Penyelesaian C**

Untuk menjalankan pengkategorian pada poin B, kami menggunakan thread sesuai petunjuk soal poin C. Berikut adalah kode yang kami gunakan.
```c
int main() {
    ...

    for (int i = 0; i < t1; i++) {
        pthread_create(&(tid[i]), NULL, processFiles, (char*)data[i]);
    }
    for(int i = 0; i < t1; i++){
        pthread_join(tid[i],NULL);
    }
}
```
Pada kode di atas kami menggunakan for loop sesuai dengan kapasitas array `data` sehingga sesuai dengan permintaan soal dimana setiap 1 file dikategorikan dengan 1 thread. Kemudian tidak lupa juga kami melakukan `pthread_join` untuk melakukan terminate thread.

### # D
Untuk mengirimkan file ke Cocoyasi Village, nami menggunakan program client-server. Saat program client dijalankan, maka folder /home/[user]/shift3/hartakarun/” akan di-zip terlebih dahulu dengan nama “hartakarun.zip” ke working directory dari program client.

**Penyelesaian D**

Pada soal ini, kita diberikan pengecualian untuk menggunakan fork() dan execv() untuk melakukan zip file. Untuk itu, kami menggunakan kode berikut untuk melakukan zipping file harta karun yang sudah dikategorikan tadi.
```c
void zipping(char *pathz) {
    int status;
    pid_t child_id;
    child_id = fork();
    if (child_id == 0) {
        char *argv[] = {"zip", "-r", "hartakarun.zip", pathz, NULL};
        execv("/usr/bin/zip", argv);
    } else {
        ((wait(&status)) > 0);
    }
}
```
Sesuai permintaan soal juga, kami menjalankan fungsi zipping tepat di awal fungsi main program kami. Berikut adalah kodenya.
```c
int main() {
    char pathz[] = "/home/ubuntu/shift3/hartakarun";
    zipping(pathz);
    ...
}
```
Berikut adalah hasil dari zip filenya.
![Hasil Zipping](image/soal3a.5.png)

### # E
Client dapat mengirimkan file “hartakarun.zip” ke server dengan mengirimkan command berikut "send hartakarun.zip" ke server

**Penyelesaian E**

Koneksi client-server sebenarnya sudah berhasil dan file sudah berhasil dikirim, hanya saja file hasil pada folder server corrupt.

### # Kendala
Kendala pengerjaan soal 3
* Masih kurang memahami socket programming sehingga problem kesulitan untuk menjawab kenapa file yang dikirim ke server corrupt
